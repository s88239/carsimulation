import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CarSimulation extends JPanel{
    private JTextField taskField, lenField, numAtBeginning, numAtIntersection;
    private JTextArea showing;
    private String output;
    public CarSimulation(){
	    //super("Car Simulation");
	    output = "";
	    //setSize(800,600);
	    //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setLayout(new BorderLayout());

	    JPanel inputPanel = new JPanel();
	    inputPanel.setLayout(new GridLayout(4,2));
	    JLabel taskLable = new JLabel("Which task you want to run (1~3)");
	    taskField = new JTextField(10);
	    inputPanel.add(taskLable);
	    inputPanel.add(taskField);

	    JLabel lenLable = new JLabel("Highway Length");
	    lenField = new JTextField(10);
	    inputPanel.add(lenLable);
	    inputPanel.add(lenField);

	    JLabel numAtBegLabel = new JLabel("The number of cars at the beginning");
	    numAtBeginning = new JTextField(10);
	    inputPanel.add(numAtBegLabel);
	    inputPanel.add(numAtBeginning);

	    JLabel numAtIntLabel= new JLabel("The numner of cars at the intersection");
	    numAtIntersection = new JTextField(10);
	    inputPanel.add(numAtIntLabel);
	    inputPanel.add(numAtIntersection);

	    add(inputPanel,BorderLayout.NORTH);

	    JPanel buttonPanel = new JPanel();
	    buttonPanel.setLayout(new FlowLayout());
	    JButton startButton = new JButton("Start Simulation");
	    startButton.addActionListener(new running());
	    buttonPanel.add(startButton);
	    JButton clearButton = new JButton("Clear");
	    clearButton.addActionListener(new clearListener());
	    buttonPanel.add(clearButton);

	    add(buttonPanel,BorderLayout.WEST);

	    showing = new JTextArea("task choose:\n1. Simulate a single-lane highway normally\n2. The first driver gets a sudden headache and slows his car down to half of the initial speed at time step 5.\n3. Adding an interchange at distance mark 50.",25,700);
	    JScrollPane scrollText = new JScrollPane(showing);
	    //scrollText.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	    //scrollText.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
	    add(scrollText,BorderLayout.SOUTH);
    }
    /*public static void main(String[] args)
    {
	CarSimulation gui = new CarSimulation();
	gui.setVisible(true);
    }*/
    private class running implements ActionListener
    {
	public void actionPerformed(ActionEvent e){
		int task, len, num, intersection=-1;
		boolean run = true;
		task = Integer.parseInt(taskField.getText());
		len = Integer.parseInt(lenField.getText());
		num = Integer.parseInt(numAtBeginning.getText());
		int[] position, vnow, v1, v2, SpeedUp;
		int total;
		if(task==3){
		    if(len<50){
			    showing.setText("If you want to execute task 3, you must input len>=50.");
			    return;
		    }
		    intersection = Integer.parseInt(numAtIntersection.getText());
		    total = num + intersection;
		}
		else total = num;
		position = new int[total];
		vnow = new int[total];
		v1 = new int[total];
		v2 = new int[total];
		SpeedUp = new int[total];
		int i, count = 0, distance = 2*len, highway, times, j, insert;
		for(times=0;run==true&&times<200;times++)
		{
		    if(task==2&&times==4) vnow[0] /= 2;//task 2 with half speed

		    distance = 2*len;//initialize distance to a big number
		    for(i=0;i<count;i++)//cars run on the highway
		    {
			if(position[i] > len) continue;//car run exceed the length
			position[i] += vnow[i];
			distance -= position[i];//the distance to the car in the front.
			if(SpeedUp[i] != 0)//already speed up and can't change velocity
			{
			    vnow[i] = v1[i];
			    v1[i] = v2[i];
			    SpeedUp[i]--;//after one second
			}
			else if(distance/2 > vnow[i] && vnow[i]!=4)//speed up
			{
			    vnow[i] = v1[i];
			    v1[i] = v2[i];
			    if(distance/2 > 4) v2[i] = 4;
			    else v2[i] = distance / 2;
			    SpeedUp[i] = 2;
			}
			else if(distance/2 < vnow[i])//speed down
			{
			    vnow[i] = v1[i];
			    v1[i] = distance / 2;
			}
			else//the same velocity
			{
			    vnow[i] = v1[i];
			    v1[i] = v2[i];
			}
			distance = position[i];
			if(i>0 && position[i]>position[i-1] && position[i-1]<len){//car accident
				output = output + "car accident happened.\n";
				run = false;
			}
		    }
		    distance -= 1;
		    if(count<total && distance >= 2)//enough safe distance
		    {
			//add new car at the beginning
			position[count] = 1;
			if(distance/2 > 4) vnow[count] = v1[count] = v2[count] = 4;
			else vnow[count] = v1[count] = v2[count] = distance/2;
			count++;
			if(task==3&&intersection!=0)
			{
			    j=0;
			    insert = count;
			    distance = 2*len;//initialize distance to a big number
			    while(j<count)//find the position to add the interchange car
			    {
				if(position[j]>50)
				{
				    distance = position[j];
				    j++;
				}
				else if(position[j]==50){
					output = output + "car accident happened.\n";//car accident
					run = false;
				}
				else
				{
				    insert = j;
				    break;
				}
			    }
			    //cars after "insert" move to the backspace
			    for(j=count;j>insert;j--)
			    {
				position[j] = position[j-1];
				vnow[j] = vnow[j-1];
				v1[j] = v1[j-1];
				v2[j] = v2[j-1];
				SpeedUp[j] = SpeedUp[j-1];
			    }
			    //add an interchange car at the position 50
			    position[insert] = 50;
			    distance -= 50;
			    if(distance/2 > 4) vnow[insert] = v1[insert] = v2[insert] = 4;
			    else vnow[insert] = v1[insert] = v2[insert] = distance/2;
			    count++;
			    intersection--;
			}
		    }
		    //print the condition of the highway		
		    i = count-1;
		    for(highway=1;run==true&&highway<=len;highway++)
		    {
			if(i>=0 && position[i] == highway)
			{
			    if(position[i]!=1) output = output + "x";
			    else output = output + ".";
			    i--;
			}
			else output = output + ".";
		    }
		    output = output + "\n";
		    showing.setText(output);// show the result on the text area
		}
		//showing.setText(output);// show the result on the text area
	}
    }
    private class clearListener implements ActionListener{
	    public void actionPerformed(ActionEvent e){
		//taskField.setText("");
		//lenField.setText("");
		//numAtBeginning.setText("");
		//numAtIntersection.setText("");
	    	output = "";
	    	showing.setText("Please input again.");
	    }
    }
}

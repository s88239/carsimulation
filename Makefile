.PHONY: clean

car.jar: CarApplet.class
	jar cvf car.jar *.class

CarApplet.class: src/CarApplet.java src/CarSimulation.java
	javac -cp src/ -d . src/CarApplet.java

clean:
	rm -f *.class
